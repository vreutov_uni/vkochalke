<?php
    function DoRouting() {
        require_once("config.php");
        require_once("php/classes/db.php");
        require_once("php/classes/user.php");
        
        require_once('php/twig_base.php');

        $klein = new \Klein\Klein();

        $user_locations = array("friends", "chat", "messages");

        try {
            $user = User::getActiveUser();
        }
        catch (UserNotFoundException $ex) {
            // cookie may be corrupted -> clear it
            User::unauthenticateUser();
            
            return $twig->render('login.twig', array(
                'title' => 'Вход'
            ));
        }
        
        require_once("php/keep_alive.php");

        $klein->respond('/', function ($request, $response) {
            $response->redirect('/users/me/')->send();
        });

        $klein->respond('/settings', function ($request, $response) use ($twig) {
            return $twig->render('user_settings.twig', array(
                'user' => User::getActiveUser(),
            ));
        });

        $klein->with('/users', function ($request) use ($klein, $twig, $user_locations) {
            
            function getUserOr404($request, $service) {
                try {
                    return User::getUserByLogin($request->login);
                }
                catch (UserNotFoundException $ex) {
                    $service->abort(404);
                }
            }

            function reflect_me($request, $response) {
                $active = User::getActiveUser()->login;
                $response->redirect(str_replace('/me/', "/$active/", $request->uri()))->send();
            }

            $klein->respond('/', function ($request, $response) use($klein, $twig) {
                return $twig->render($request->location . "users.twig", array(
                    'users' => User::allUsers(),
                ));
            });

            $klein->respond('/[:login]', function ($request, $response) use($klein, $twig) {
                $response->redirect($request->uri() . '/')->send();
            });

            $klein->respond('/me/', reflect_me);
            $klein->respond('/me/[*]', reflect_me);

            $klein->respond('/[:login]/', function ($request, $response) use($klein, $twig) {
                $user = getUserOr404($request, $klein);
    
                return $twig->render('profile.twig', array(
                    'user' => $user,
                    'active_user' => User::getActiveUser(),
                ));
            });

            $klein->respond('/[:login]/[:location]', function ($request, $response) use($klein, $twig, $user_locations) {
                $user = getUserOr404($request, $klein);
                
                if (!in_array($request->location, $user_locations))
                    $klein->abort(404);

                try {
                    return $twig->render($request->location . ".twig", array(
                        'user' => $user,
                        'active_user' => User::getActiveUser(),
                    ));
                }
                catch (Twig_Error_Loader $err) {
                    $klein->abort(404);
                }
            });
        });

        $klein->onHttpError(function ($code, $router) use($twig) {
            switch ($code) {
                case 404:
                    $router->response()->body(
                        $twig->render('404.twig', array())
                    );
                    break;
                default:
                    $router->response()->body(
                        'Oh no, a bad error happened that caused a '. $code
                    );
            }
        });

        $klein->dispatch();
    }

    echo DoRouting();
?>