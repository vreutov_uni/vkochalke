<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);
    require_once("$root/php/classes/db.php");

    class UserNotFoundException extends Exception { }
    class InvalidLoginException extends Exception { }
    class InvalidPasswordException extends Exception { }

    class User {
        public $userData = null;

        public $id;
        public $login;
        public $name;
        private $key;

        private static $allUsersQuery = 
            "SELECT koch.id, login, salt, CONCAT(koch.firstName, ' ',  koch.lastName) as name, koch.*
            FROM Users user JOIN Kochs koch ON user.idKoch = koch.id ";

        function __construct($data) {
            $this->userData = $data;

            $this->id = $data["id"];
            $this->name = $data["name"];
            $this->login = $data["login"];
            $this->key = self::getKey($data);
        }

        private static function getUserByCondition($condition,  ...$params) {
            $res = DB::query_p(self::$allUsersQuery . "WHERE " . $condition, ...$params);
            
            if ($data = $res->fetch_assoc()) {
                return new User($data);
            } else {
                throw new UserNotFoundException();
            }
             
        }

        public static function allUsers() {
            $res = DB::query_p(self::$allUsersQuery);
            
            return array_map(function($row) {
                return new User($row);
            }, $res->fetch_all(MYSQLI_ASSOC));
        }

        public static function getActiveUser() {
            $user = self::getUserById($_COOKIE["kochid"]);
            if (!$user->isActive()) {
                throw new UserNotFoundException();
            }
            return $user;
        }

        public static function getUserByLogin($userLogin) {
            return self::getUserByCondition("login = '%0%'", $userLogin);
        }

        public static function getUserById($kochId) {
            return self::getUserByCondition("koch.id = %0%", $kochId);
        }

        private static function hashPassword($pass, $salt) {
            return (string)hash("sha256", $salt . $pass . $salt);
        }

        public static function getKey($userData) {
            return hash("sha256", $userData['login'].$userData['salt']);
        }

        public static function authenticateUser($login, $password) {
            $result = DB::query_p("SELECT salt FROM Users WHERE login = '%0%'", $login);
            if (!$salt_row = $result->fetch_row())
                throw new InvalidLoginException();

            $salt = $salt_row[0];
            $hashedPass = self::hashPassword($password, $salt);
            $result = DB::query_p(
                "SELECT id, login, idKoch, salt FROM Users WHERE login = '%0%' AND password = '%1%'",
                $login, $hashedPass
            );
            
            if ($user = $result->fetch_assoc()) {
                $exp_time = time() + 60*60*24*7;
                setcookie("userid", $user['id'], $exp_time, '/');
                setcookie("login", $user['login'], $exp_time, '/');
                setcookie("kochid", $user['idKoch'], $exp_time, '/');
                setcookie("key", self::getKey($user), $exp_time, '/');
            } else {
                throw new InvalidPasswordException();
            }
        }

        public static function verifyUser($kochId, $userKey)
        {
            return self::getUserById($kochId)->key == $userKey;
        }

        public static function unauthenticateUser() {
            setcookie("userid", null, -1, '/');
            setcookie("kochid", null, -1, '/');
            setcookie("login",  null, -1, '/');
            setcookie("key",    null, -1, '/');
        }

        public static function register($login, $pass, $lname, $fname) {
            DB::query_p(
                "INSERT INTO Kochs (firstName, lastName) VALUES ('%0%', '%1%')",
                $lname, $fname
            );

            $kochId = DB::getMySQLiObject()->insert_id;

            $salt = md5($login."SomeSuperSalt");

            DB::query_p(
                "INSERT INTO Users (idKoch, login, password, salt) VALUES ('%0%', '%1%','%2%', '%3%')",
                $kochId, $login, self::hashPassword($pass, $salt), $salt
            );
        }

        public function isActive() {
            return $_COOKIE["key"] == $this->key;
        }

        public function isOnline() {
            $res = DB::query_p("
                SELECT TIME_TO_SEC(TIMEDIFF(NOW(), lastActive)) < 120 as active
                FROM Users WHERE login = '%0%'", $this->login);

            return $res->fetch_row()[0] == 1;
        }

        public function dialogs() {
            $result = DB::query_p(
                "SELECT login, firstName, lastName, Kochs.id, ava, m1.idKochFrom, idKochTo, date, text, m2.unreaded 
                FROM Messages as m1
                INNER JOIN Kochs ON m1.id=
                    (SELECT id FROM Messages
                    WHERE ( Kochs.id = Messages.idKochTo AND Messages.idKochFrom = %0%)
                    OR ( Kochs.id = Messages.idKochFrom AND Messages.idKochTo = %0%)
                    ORDER BY date DESC LIMIT 1) 
                    JOIN Users ON Users.idKoch = Kochs.id
                LEFT JOIN               
                (SELECT idKochFrom, COUNT(isReaded) unreaded 
                FROM Messages 
                WHERE idKochTo = %0% 
                AND isReaded = 0
                GROUP BY idKochFrom) m2 
                ON m2.idKochFrom = m1.idKochFrom               
                ORDER BY date DESC", $this->id);
                
            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function posts() {
            $result = DB::query_p(
                "SELECT p.*, k.ava, CONCAT(k.firstName, ' ',  k.lastName) as name, COUNT(l.id) as likes
                FROM Posts p
                JOIN Kochs k ON idAutor = k.id 
                LEFT JOIN Likes l ON l.idPost = p.id
                WHERE p.idKoch = %0% AND isDeleted = 0
                GROUP BY p.id
                ORDER BY date DESC", $this->id);
                
            return $result->fetch_all(MYSQLI_ASSOC);            
        }
        
        public function likes() {
            $result = DB::query_p(
                "SELECT idPost FROM Likes
                    WHERE idKoch = %0%", $this->id
            );
            return array_map(function($row){ return $row[0]; }, $result->fetch_all());

        }

        public function base_stats() {
            // placed in display order
            return array(
                'height' => array('name' => 'Рост', 'value' => 'см'),
                'weight' => array('name' => 'Вес', 'value' => 'кг'),
                'benchPress' => array('name' => 'Жим', 'value' => 'кг'),
                'squat' => array('name' => 'Присед', 'value' => 'кг'),
                'deadlift' => array('name' => 'Становая', 'value' => 'кг'),
            );
        }

        public function main_info() {
            return array(
                'firstName' => 'Имя',
                'lastName' => 'Фамилия',
                'sex' => 'Пол',
                'birthday' => 'Дата рождения',
            );
        }

        public function other_stats() {
            return array('about' => array('name' => 'Коротко о себе', 'value' => ''));
        }

        public function stats() {
            return $this->base_stats() + $this->other_stats();
        }

        public function friends() {
            $q = self::$allUsersQuery
                .
                "JOIN Friends f1 on koch.id = f1.idKoch2
                LEFT JOIN Friends f2
                ON f1.idKoch1 = f2.idKoch2 AND f1.idKoch2 = f2.idKoch1
                WHERE f2.id IS NOT NULL AND f1.idKoch1 = %0%";

            $res = DB::query_p($q, $this->id);

            $friends = array();
            while ($data = $res->fetch_assoc()) {
                $friends[] = new User($data);
            }

            return $friends;
        }

        public function hasFriendRequests() {
            $res = DB::query_p(
                "SELECT COUNT(*) > 0 as hasRequests FROM Friends f1
                LEFT JOIN Friends f2
                ON f1.idKoch1 = f2.idKoch2 AND f1.idKoch2 = f2.idKoch1
                WHERE f2.id IS NULL AND f1.idkoch2 = %0%", $this->id);
            
            return $res->fetch_row()[0];
        }

        public function hasOutFriendRequests() {
            $res = DB::query_p(
                "SELECT COUNT(*) > 0 as hasRequests FROM Friends f1
                LEFT JOIN Friends f2
                ON f1.idKoch1 = f2.idKoch2 AND f1.idKoch2 = f2.idKoch1
                WHERE f2.id IS NULL AND f1.idkoch1 = %0%", $this->id);
            
            return $res->fetch_row()[0];
        }

        public function friendRequests() {
            $res = DB::query_p(
                "SELECT f1.idKoch1 as hasRequests FROM Friends f1
                LEFT JOIN Friends f2
                ON f1.idKoch1 = f2.idKoch2 AND f1.idKoch2 = f2.idKoch1
                WHERE f2.id IS NULL AND f1.idkoch2 = %0%", $this->id);

            return array_map(function($row) {
                return self::getUserById($row[0]);
            }, $res->fetch_all());
        }

        public function outFriendRequests() {
            $res = DB::query_p(
                "SELECT f1.idKoch2 as hasRequests FROM Friends f1
                LEFT JOIN Friends f2
                ON f1.idKoch1 = f2.idKoch2 AND f1.idKoch2 = f2.idKoch1
                WHERE f2.id IS NULL AND f1.idkoch1 = %0%", $this->id);

            return array_map(function($row) {
                return self::getUserById($row[0]);
            }, $res->fetch_all());
        }
        

        public function isRequestingFriend() {
            $res = DB::query_p(
                "SELECT COUNT(*) > 0 as hasRequests FROM Friends f1
                LEFT JOIN Friends f2
                ON f1.idKoch1 = f2.idKoch2 AND f1.idKoch2 = f2.idKoch1
                WHERE f2.id IS NULL AND f1.idkoch2 = %0% AND f1.idKoch1 = %1%", User::getActiveUser()->id, $this->id);
            
            return $res->fetch_row()[0];
        }

        public function isFriendToActive() {
            $res = DB::query_p(
                "SELECT COUNT(*) = 2 as isFriend FROM Friends f
                WHERE (f.idKoch1 = %0% AND f.idKoch2 = %1%)
                OR (f.idKoch2 = %0% AND f.idKoch1 = %1%)"
                , User::getActiveUser()->id, $this->id);
            
            return $res->fetch_row()[0];
        }

        public function isRequestedFriend() {
            $res = DB::query_p(
                "SELECT COUNT(*) > 0 as hasRequests FROM Friends f1
                LEFT JOIN Friends f2
                ON f1.idKoch1 = f2.idKoch2 AND f1.idKoch2 = f2.idKoch1
                WHERE f2.id IS NULL AND f1.idkoch1 = %0% AND f1.idKoch2 = %1%", User::getActiveUser()->id, $this->id);
            
            return $res->fetch_row()[0];
        }

        // TODO: delete this shit, kept for now for backwards compatibility
        public function getPosts() {
            $posts = $this->posts();

            require('twig_base.php');

            $res = "";
            foreach ($posts as $post) {
                $res .= $twig->render('user_post.html', array(
                    'user' => $this,
                    'post' => $post,
                ));
            }
            
            return $res;
        }
    }
?>