<?php
	$root = realpath($_SERVER["DOCUMENT_ROOT"]);
	require_once("$root/config.php");

	DB::init($dbOptions);
	
class DB {
	private static $instance;
	private static $options;
	private $MySQLi;
	
	private function __construct(array $dbOptions){

		$this->MySQLi = @ new mysqli(	$dbOptions['db_host'],
						$dbOptions['db_user'],
						$dbOptions['db_pass'],
						$dbOptions['db_name'] );

		if (mysqli_connect_errno()) {
			throw new Exception('Ошибка базы данных.');
		}

		self::$options = $dbOptions;
		$this->MySQLi->set_charset("utf8");
	}
	
	public static function init(array $dbOptions){
		if(self::$instance instanceof self){
			return false;
		}
		
		self::$instance = new self($dbOptions);
	}

	public static function ping() {
		if (!self::$instance->MySQLi->ping()) {
			self::$instance = NULL;
			self::init(self::$options);
		}
	}
	
	public static function getMySQLiObject(){
		return self::$instance->MySQLi;
	}
	
	public static function query($q){
		$res = self::$instance->MySQLi->query($q);

		if (!$res) {
			$message  = 'Error while executing query: ' . self::$instance->MySQLi->error . "; \n";
			$message .= 'Query: ' . $q;
			die($message);
		}

		return $res;
	}
	
	/**
	 * Параметризированный запрос к бд
	 * 
	 * @param $q - строка запроса. Параметры запроса в ней обозначены в формате %i%, i = 0...
	 * @param $params - параметры запроса. По количеству должны совпадать с
	 * 	количеством различными %i% в запросе
	 * 
	 * @return объект mysql_result, если он не равен FALSE
	 */
	public static function query_p($q, ...$params)
	{
		$i = 0;
		foreach ($params as $param) {
			if ($param == NULL)
				$param = 'NULL';

			$q = str_replace("%$i%", DB::esc($param), $q);
			$i++;
		}

		return self::query($q);
	}
	
	public static function esc($str){
		return self::$instance->MySQLi->real_escape_string(htmlspecialchars($str));
	}
}
?>