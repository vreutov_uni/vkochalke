<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);

require_once "$root/vendor/autoload.php";
require_once "$root/php/classes/user.php";

use Workerman\Worker;

$users = [];

$ws_worker = new Worker($notify_server_addr);

$ws_worker->onWorkerStart = function() use (&$users)
{
    global $local_socket_addr;
    $inner_tcp_worker = new Worker($local_socket_addr);

    $inner_tcp_worker->onMessage = function($connection, $data) use (&$users) {
        $data = json_decode($data);
        if (isset($users[$data->kochId])) {
            foreach ($users[$data->kochId] as $webconnection) {
                $webconnection->send($data->message);
            }
        }
    };
    $inner_tcp_worker->listen();
};

$ws_worker->onConnect = function($connection) use (&$users)
{
    $connection->onWebSocketConnect = function($connection) use (&$users)
    {
        $id = $_COOKIE['kochid'];
        $key = $_COOKIE['key'];
        
        DB::ping();

        if (User::verifyUser($id, $key)) {
            if (isset($users[$id])) {
                $users[$id][] = $connection;
            } else {
                $users[$id] = array($connection);
            }
        }
    };
};

$ws_worker->onClose = function($connection) use(&$users)
{
    foreach ($users as $userId => $connections) {
        if ($conn = array_search($connection, $connections)) {
            unset($users[$userId][$conn]);
            break;
        }
    }    
};

Worker::runAll();
?>