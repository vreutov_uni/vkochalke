<?php
require("../../config.php");
require("../classes/db.php");
require("../classes/user.php");

DB::init($dbOptions);

$activeUser = User::getActiveUser();
$companion = User::getUserByLogin($_GET["companion"]);

if ($activeUser && $companion)
{
    $date = date("Y-m-d H:i:s");
    $text = DB::esc($_GET["text"]);
    $time = date('H:i', strtotime($date));
    DB::query_p(
        "INSERT INTO Messages (idKochFrom, idKochTo, date, text, isDeleted)
        VALUES(%0%, %1%, '%2%', '%3%', false)", $activeUser->id, $companion->id, $date, $_GET["text"]);
    
    $idMessage = DB::getMySQLiObject()->insert_id;
    
    $ftp_socket = stream_socket_client($local_socket_addr);

    $message = json_encode([
        'message' => "<div class='notMe'><p class='msgText'>$text</p><p class='msgTime'>$time</p></div>\n",
        'type' => 'message',
        'from' => "$activeUser->name",
        'href' => "/users/$activeUser->login/chat",
        'id' => "$idMessage"
    ]);
    fwrite($ftp_socket, json_encode([
        'kochId' => $companion->id,
        'message' => $message
    ])  . "\n");


    echo json_encode(['status' => 'ok', 'message' => "<div class='me'><p class='msgText'>$text</p><p class='msgTime'>$time</p></div>\n"]);
}
else
{
    echo json_encode(['status' => 'failed']);
}
?>