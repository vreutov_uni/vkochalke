<?php
require("../../config.php");
require("../classes/db.php");
require("../classes/user.php");

$activeUser = User::getActiveUser();
$companion = User::getUserByLogin($_GET["companion"]);

if ($activeUser && $companion)
{
    $res = DB::query(
        "SELECT * FROM Messages
        WHERE isDeleted = false AND (idKochFrom = " . $activeUser->id . " AND idKochTo = " . $companion->id .
        " OR idKochFrom = " . $companion->id . " AND idKochTo = " . $activeUser->id . ")" .
        " ORDER BY date");
    DB::query(
        "UPDATE Messages SET isReaded = 1
        WHERE isReaded = 0 AND (idKochFrom = " . $activeUser->id . " AND idKochTo = " . $companion->id .
        " OR idKochFrom = " . $companion->id . " AND idKochTo = " . $activeUser->id . ")"
    );
    if ($res) {
        while ($row = $res->fetch_assoc()) {
            $text = $row["text"];
            $class = ($row["idKochFrom"] == $activeUser->id) ? "me" : "notMe";
            $strTime = strtotime($row["date"]);
            $today = date("d.m.Y", time());
            if ($today === date("d.m.Y",$strTime))
                $time = date('H:i',$strTime);
            else
                $time = date( 'H:i d.m.y', $strTime);
        
            echo "<div class='$class'><p class='msgText'>$text</p><p class='msgTime'>$time</p></div>\n";

        }
    } else {
        echo "Failed to get messages";
    }
} else {
    echo "User or companion not found";
}
?>