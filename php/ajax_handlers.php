<?php
    require_once("classes/db.php");
    require_once("classes/user.php");
    require_once("twig_base.php");

    function vk_handle_login() {
        try {
            User::authenticateUser($_POST['login'], $_POST['pass']);
            echo "OK";
        }
        catch (InvalidLoginException $ex) {
            echo "Пользователь с таким логином не найден";
        }
        catch (InvalidPasswordException $ex) {
            echo "Неверный логин / пароль";
        }
    }

    function vk_handle_logout() {
        User::unauthenticateUser();
        echo "logout";
        echo "OK";
    }

    function vk_handle_check_login() {
        $res = DB::query_p("SELECT * FROM Users WHERE login = '%0%'", $_POST['login']);
        if (!$res->fetch_row() && $login != 'me') {
            echo "OK";
        } else {
            echo "EXIST";
        }
    }

    function vk_handle_register() {
        User::register($_POST['login'], $_POST['pass'], $_POST['name'], $_POST['fam']);
        echo "OK";
    }

    function vk_handle_new_post() {
        $active = User::getActiveUser();
        $other = User::getUserByLogin($_POST['user']);

        if (!$other->isActive() && !$other->isFriendToActive())
            return;

        DB::query_p(
            "INSERT INTO `Posts` (`idKoch`, `idAutor`, `date`, `text`, `isDeleted`)
            VALUES (%0%, %1%, '%2%', '%3%', 0)",
            $other->id, $active->id, date("Y-m-d H:i:s",time()), $_POST['text']
        );

        $postId = DB::getMySQLiObject()->insert_id;
        $res = DB::query_p(
            "SELECT p.*, k.ava, CONCAT(k.firstName, ' ',  k.lastName) as name, COUNT(l.id) as likes
            FROM Posts p
            JOIN Kochs k ON idAutor = k.id 
            LEFT JOIN Likes l ON l.idPost = p.id
            WHERE p.id = %0%
            GROUP BY p.id
            ORDER BY date DESC", $postId);
        $post = $res->fetch_assoc();

        global $twig;
        echo $twig->render('user_post.twig', array(
            'post' => $post,
            'active_user' => User::getActiveUser()
        ));
        
        if ($post['idAutor'] != $other->id){
            
            global $local_socket_addr;
            $ftp_socket = stream_socket_client($local_socket_addr);

            $message = json_encode([
                'message' => "Новая запись на стене от <br>$active->name",
                'type' => 'post'
            ]);

            fwrite($ftp_socket, json_encode([
                'kochId' => $other->id,
                'message' => $message
            ])  . "\n");
        }
    }

    function vk_handle_del_post() {
        $active = User::getActiveUser();

        $res = DB::query_p("SELECT * FROM Posts WHERE id = %0%", $_POST['id']);

        if ($post = $res->fetch_assoc())
        {
            if ($active->id == $post['idKoch'] || $active->id == $post['idAutor']) {
                DB::query_p("UPDATE Posts SET isDeleted = 1 WHERE ID = %0%", $_POST['id']);
            } else {
                // НИЗЯ
            }
        }
    }

    function vk_handle_like_post() {
        $active = User::getActiveUser();
        $res = DB::query_p("SELECT * FROM Likes WHERE idPost = %0% AND idKoch = %1%", $_POST['id'], $active->id);
        $like = $res->fetch_assoc();
        $res = DB::query_p("SELECT idAutor FROM Posts WHERE id = %0%", $_POST['id']);
        $owner = $res->fetch_assoc();

        if ($like)
            DB::query_p(
                "DELETE FROM Likes WHERE id = %0%", $like['id']
            );
        else
        {
            DB::query_p(
                "INSERT INTO Likes (`idPost`, `idKoch`)
                VALUES (%0%, %1%)",
                $_POST['id'], $active->id
            );
            
            if ($owner['idAutor'] != $active->id){
                global $local_socket_addr;
                $ftp_socket = stream_socket_client($local_socket_addr);

                $message = json_encode([
                    'message' => "Лайк от <br>$active->name",
                    'type' => 'like'
                ]);

                fwrite($ftp_socket, json_encode([
                    'kochId' => $owner['idAutor'],
                    'message' => $message
                ])  . "\n");
            }
        }       
        $likesCount = DB::query_p(
            "SELECT COUNT(l.id) as likes
            FROM Posts p
            JOIN Kochs k ON idAutor = k.id 
            LEFT JOIN Likes l ON l.idPost = p.id
            WHERE p.id = %0%", $_POST['id']);
        
        $likes = $likesCount->fetch_assoc();
        echo " ".$likes['likes'];
    }

    function vk_handle_readed_message(){
        DB::query_p(
            "UPDATE Messages SET isReaded = 1
            WHERE isReaded = 0 AND id = %0%", $_POST['id']);
    }

    function vk_handle_save_about() {
        $active = User::getActiveUser();
        $data = date("Y-m-d",strtotime($_POST['birthday']));

        $info_to_save = "height='%1%', weight='%2%', benchPress='%3%', squat='%4%', 
        deadlift='%5%', about='%6%'";

        if (isset($_POST['firstName'])) {
            $info_to_save .= ", firstName='%7%', lastName='%8%', sex='%9%', birthday='%10%'";
        }

        DB::query_p(
            "UPDATE Kochs SET $info_to_save where id = %0%", $active->id,
            $_POST['height'], $_POST['weight'], $_POST['benchPress'],
            $_POST['squat'], $_POST['deadlift'], $_POST['about'],
            $_POST['firstName'], $_POST['lastName'], $_POST['sex'], $data);
    }

    function vk_handle_get_notify_server() {
        global $notify_server_client_addr;
        echo $notify_server_client_addr;
    }

    function do_friend_request() {
        $friend = User::getUserById($_POST['userId']);
        $active = User::getActiveUser();
        
        $res = DB::query_p(
            "SELECT * FROM Friends WHERE idKoch1 = %0% AND idKoch2 = %1%",
            $active->id, $friend->id);
        
        if (!$res->fetch_row()) {
            DB::query_p(
                "INSERT INTO Friends (idKoch1, idKoch2)
                VALUES(%0%, %1%)", $active->id, $friend->id);
        }

        echo "OK";
    }

    function vk_handle_accept_request() {
        $friend = User::getUserById($_POST['userId']);

        if (!$friend->isRequestingFriend())
            return;

        do_friend_request();
        $active = User::getActiveUser();
        
        global $local_socket_addr;
        $ftp_socket = stream_socket_client($local_socket_addr);

        $message = json_encode([
            'message' => "$active->name тоже хочет дружить!",
            'type' => 'friend'
        ]);

        fwrite($ftp_socket, json_encode([
            'kochId' =>  $friend->id,
            'message' => $message
        ])  . "\n");
    }

    function vk_handle_decline_request() {
        $friend = User::getUserById($_POST['userId']);
        $active = User::getActiveUser();
        
        if ($friend->isRequestingFriend())
        {
            DB::query_p(
                "DELETE FROM Friends WHERE idKoch1 = %1% AND idKoch2 = %0%",
                $active->id, $friend->id);
        }

        echo "OK";
    }

    function vk_handle_make_request() {
        do_friend_request();
        $friend = User::getUserById($_POST['userId']);
        $active = User::getActiveUser();

        global $local_socket_addr;
        $ftp_socket = stream_socket_client($local_socket_addr);

        $message = json_encode([
            'message' => "$active->name хочет дружить!",
            'type' => 'friend'
        ]);

        fwrite($ftp_socket, json_encode([
            'kochId' =>  $friend->id,
            'message' => $message
        ])  . "\n");
    }

    function vk_handle_reset_request() {
        $friend = User::getUserById($_POST['userId']);
        $active = User::getActiveUser();

        if ($friend->isRequestedFriend()) {
            DB::query_p(
                "DELETE FROM Friends WHERE idKoch1 = %0% AND idKoch2 = %1%",
                $active->id, $friend->id);
        }

        echo "OK";
    }

    function vk_handle_remove_friend() {
        $friend = User::getUserById($_POST['userId']);
        $active = User::getActiveUser();

        DB::query_p(
            "DELETE FROM Friends
            WHERE (idKoch1 = %0% AND idKoch2 = %1%)
            OR (idKoch1 = %1% AND idKoch2 = %0%)",
            $active->id, $friend->id);

        echo "OK";
    }

    function vk_handle_request_new_friend_tile() {
        $friend = User::getUserById($_POST['userId']);

        $curUser = NULL;
        if ($_POST['curUser'] != "") {
            $curUser = User::getUserByLogin($_POST['curUser']);
        } 

        if (!$curUser || !$curUser->isActive()) {
            global $twig;
            echo $twig->render('friend_tile.twig', array(
                'user' => $curUser,
                'friend' => $friend));
        }
    }

    function vk_handle_request_actions_block() {
        $curUser = User::getUserByLogin($_POST['curUser']);

        global $twig;
        echo $twig->render('actions_block.twig', array(
            'user' => $curUser));
    }
?>