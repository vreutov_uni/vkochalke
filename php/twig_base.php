<?php
    $root = realpath($_SERVER["DOCUMENT_ROOT"]);

    require_once("$root/vendor/autoload.php");
    require_once("$root/php/twig_extensions.php");

    $loader = new Twig_Loader_Filesystem("$root/templates");
    $twig = new Twig_Environment($loader, array(
        'cache' => "$root/templates/cache",
        'debug' => true,
    ));

    $twig->addExtension(new Statics_Extension());
?>
