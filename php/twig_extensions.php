<?php

class Statics_Extension extends Twig_Extension implements Twig_Extension_GlobalsInterface
{
    public $statics = array(
        'root'  => '/static/',
        'js'    => '/static/js',
        'css'   => '/static/css',
        'img'   => '/static/images',
        'fonts' => '/static/fonts',
    );

    public function getFilters()
    {
        return array(
            new Twig_Filter('static', array($this, 'staticFilter')),
        );
    }

    public function getGlobals()
    {
        return array(
            'statics' => $statics,
        );
    }

    public function staticFilter($literal, $type)
    {
        return $this->statics[$type] . "/" . $literal;
    }
}

?>