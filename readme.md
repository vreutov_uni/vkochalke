Такс, у вас ничего просто так не заработает, потому что тут теперь шаблоны.
Нужно поставить движок шаблонов, но чтобы его установить, сначала нужно установить менеджер пакетов для php.
Крч:
    1. Устанавливаете это: https://getcomposer.org/Composer-Setup.exe
    2. Запускаете командную строку в папке проекта и пишете туда composer update
    3. Пьете чаек около 30 секунд, пока все загрузится
    4. Вы великолепны, все работает


Как запустить сервер оповещений:
    0. composer update

    1. OpenServer -> ПКМ -> Консоль
    2. cd domains/%имя папки с сайтом%
    3. php -q .\php\notify-server.php
    4. Не закрывать консоль

    ИЛИ
    1. OpenServer -> ПКМ -> Консоль
    2. Автозагрузка
    3. Выбрать файл run-server.bat
    4. При запуске консоль запустится сама
    (Именно при запуске самого openserver.exe, а не локального сервера)
