$(document).ready(function() {

    var isBlank = function(obj){
        return(!obj || $.trim(obj) === "");
    };

    var getUser = function() {
        var match = window.location.pathname.match("/users/([^/]+)/");
        if (match) {
            return match[1];
        } else {
            return null;
        }
    }

    $(".exitbutton").click(function(){
        $.post('/php/ajax.php', { action: "logout"}, function() {
            location.replace("/");
        });        
    });

    var hide_new_requests = function() {
        $("#new-requests").removeClass("pressed");
        $("#new-requests-block").addClass("element_hide").removeClass("ui cards");
    }

    var hide_out_requests = function() {
        $("#out-requests").removeClass("pressed");
        $("#out-requests-block").addClass("element_hide").removeClass("ui cards");
    }

    $("#new-requests").click(function(){
        if ($("#new-requests-block").hasClass("element_hide")) {
            hide_out_requests();
            $("#new-requests").addClass("pressed");
            $("#new-requests-block").addClass("ui cards").removeClass("element_hide");
        } else {
            hide_new_requests();
        }
    });

    $("#out-requests").click(function(){
        if ($("#out-requests-block").hasClass("element_hide")) {
            hide_new_requests();
            $("#out-requests").addClass("pressed");
            $("#out-requests-block").addClass("ui cards").removeClass("element_hide");
        } else {
            hide_out_requests();
        }
    });

    var getTileFromButton = function (el) {
        while ((el = el.parentElement) && !("id" in el.dataset));
        return el;
    }

    var getIdFromCardButton = function(el) {
        return getTileFromButton(el).dataset.id;
    }

    var getIdFromE = function(e) {
        return getIdFromCardButton(e.target || e.srcElement);
    }

    $(".make-req").click(function(e) {
        var userId = getIdFromE(e);

        alert(userId);
    });

    var friend_actions = ["accept_request", "decline_request", "reset_request", "make_request", "remove_friend"];
    var need_confirm = ["remove_friend"];

    $(".ui.basic.modal").modal({
            closable: true
    });

    function bindActions($scope) {
        friend_actions.forEach(function(action) {
            $scope.find(`.${action}`).click(function(e) {
                var tile = getTileFromButton(e.target || e.srcElement);
                var userId = getIdFromE(e);
                
                var delete_friend = function() {
                    $.post('/php/ajax.php', {
                        action: action,
                        userId: userId,
                        curUser: getUser(),
                    }, function (response) {        
                        if (response === "OK") {
                            $.post('/php/ajax.php', {
                                action: "request_new_friend_tile",
                                userId: userId,
                                curUser: getUser(),
                            }, function (response) {        
                                if (response !== "") {
                                    var div = document.createElement('div');
                                    div.innerHTML = response;
                                    bindActions($(div.firstChild));
                                    tile.parentNode.replaceChild(div.firstChild, tile);
                                } else {
                                    $(tile).fadeOut('slow', function() {
                                        tile.parentNode.removeChild(tile);
                                        checkEmptyFriendsContainers();
                                    });
                                }
                            });
                        }
                    });
                }

                $(".ui.basic.modal").modal({
                        closable: true,
                        onApprove: delete_friend
                });
        

                if (action === "remove_friend") {
                    $('.ui.basic.modal').modal('show');
                } else {
                    delete_friend();
                }
            });
        });
    }

    var user_actions = friend_actions;

    function bindCurrentUserActions($scope) {
        friend_actions.forEach(function(action) {
            $scope.find(`.${action}`).click(function(e) {
                var userId = getIdFromE(e);
        
                $.post('/php/ajax.php', {
                    action: action,
                    userId: userId,
                    curUser: getUser(),
                }, function (response) {        
                    if (response === "OK") {
                        $.post('/php/ajax.php', {
                            action: "request_actions_block",
                            curUser: getUser(),
                        }, function (response) {        
                            $("#actions-block").html(response);
                            bindCurrentUserActions($("#actions-block"));
                        });
                    }
                });
            });
        });
    }

    bindActions($(".friendswrap"));
    bindCurrentUserActions($("#actions-block"));

    function checkEmptyFriendsContainers() {
        var hasOut = $("#out-requests-block").has(".friend-tile").length !== 0;
        var hasIn = $("#new-requests-block").has(".friend-tile").length !== 0;
        
        if (!hasOut && !hasIn) {
            $("#actions-block").addClass("element_hide");
            hide_new_requests();
            hide_out_requests();
        } else if (!hasOut) {
            hide_out_requests();
            $("#out-requests").addClass("element_hide");
        } else if (!hasIn) {
            hide_new_requests();
            $("#new-requests").addClass("element_hide");
        }
    }

    $(".postbutton").click(function(){
        if (isBlank($("#inputposttext").val()))
            return;
        $.ajax({
            method: "post",
            url: "/php/ajax.php",
            data: {
                action: "new_post",
                text: $("#inputposttext").val(),
                user: getUser(),
            },
            beforeSend: function(){
                // $("#output").text("LOADING");
            },
            success: function(data){
                $("#inputposttext").val("");
                $("#output").text("");
                $("#posts").prepend(data);
            },
            error: function(err){
                $("#output").text(err);
            }
        });
    });

    $(document).on('click','.delpost', function() {
        $postId = $(this).attr('data-id');
        $.ajax({
            method: "post",
            url: "/php/ajax.php",
            data: {
                id: $postId,
                action: "del_post",
            },
            beforeSend: function(){
                // $("#output").text("LOADING");
            },
            success: function(){
                $("#output").text("");
                $("[data-id^="+$postId+"]").detach();
            },
            error: function(err){
                $("#output").text(err);
            }
        });
    });

    $(document).on('click','.like', function() {
        $like = $(this);
        $.ajax({
            method: "post",
            url: "/php/ajax.php",
            data: {
                id: $like.attr('data-id'),
                action: "like_post"
            },
            beforeSend: function(){
                // $("#output").text("LOADING");
            },
            success: function(data){
                if ($like.css('color') === 'rgb(0, 0, 0)')
                    $like.css({'color': 'red'});
                else
                    $like.css({'color': 'black'});
                $like.text(data);
            },
            error: function(err){
                $("#output").text(err);
            }
        });
    });

    setInterval(function () {
        $.get("/php/keep_alive.php");
    }, 120000);
});