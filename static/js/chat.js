$(document).ready(function () {

    var getUser = function() {
        var match = window.location.pathname.match("/users/([^/]+)/");
        if (match) {
            return match[1];
        } else {
            return null;
        }
    }

    var chatRefreshInterval = 1000;
    var $chatOutput = $("#chatOutput");
    var $chatInput = $("#chatInput");
    var $chatSend = $("#chatSend");
    
    function sendMessage() {
        var chatInputString = $chatInput.val().trim();

        if (chatInputString.trim().length > 0) {
            $.get("/php/chat/write.php", {
                companion: getUser(),
                text: chatInputString
            }, function (response) {
                var data = JSON.parse(response);
    
                if (data.status == "ok") {
                    $chatInput.val("");
                    $chatOutput.append(data.message);
                } else {
                    alert("Ошибка отправки сообщения");
                }
            });
        }
    }

    function retrieveMessages(refresh = false) {

        $.get("/php/chat/read.php", {
            companion: getUser()
        }, function (data) {
            refresh = refresh || ($chatOutput[0].scrollHeight - $chatOutput.scrollTop() - $chatOutput.outerHeight() < 10)

            $chatOutput.html(data);
            if (refresh)
                refreshOutput();
        });
    }

    function refreshOutput() {
        $chatOutput.stop().animate({
            scrollTop: $chatOutput[0].scrollHeight
        }, 400);
    }

    $chatSend.click(function () {
        sendMessage();
        refreshOutput();
    });

    document.querySelector("#chatInput").addEventListener("keyup", function (e) {
        if (e.key == "Enter") {
            document.querySelector('#chatSend').click();
            e.preventDefault();
        }
    });
    
    $chatOutput.scroll(function(){
        $innerHeight = $("#chatOutput")[0].scrollHeight - $("#chatOutput")[0].scrollTop - $("#chatOutput").outerHeight();
    
        $notified = document.getElementById("notify-message");
        
        if ($notified && $innerHeight <= 0) {
            $notified.remove();
        }
    });
    
    retrieveMessages(true);
});