$.post("/php/ajax.php", {
    action: "get_notify_server"
}, function (response) {
    setup_notifies(response);
});

function setup_notifies(server_addr) {
    ws = new WebSocket(server_addr);
    
    ws.onerror = function(event) {
        if (ws.readyState !== ws.OPEN) {
            alert("Сервер оповещений не запущен! См. readme");
        }
    
        // Unknown error
    }
    
    ws.onmessage = function(event) {
        var data = JSON.parse(event.data);
        switch (data.type) {
            case "message":
                var chatOutput = $("#chatOutput");
                
                if (chatOutput[0]) {
                    var refresh = chatOutput[0].scrollHeight - chatOutput.scrollTop() - chatOutput.outerHeight() < 10;
                    chatOutput.append(data.message);
                    
                    $.ajax({
                        method: "post",
                        url: "/php/ajax.php",
                        data: {
                            id: data.id,
                            action: "readed_message",
                        },
                        error: function(err){
                            $("#output").text(err);
                        }
                    });

                    if (refresh) {
                        chatOutput.stop().animate({
                            scrollTop: chatOutput[0].scrollHeight
                        }, 400);
                        break;
                    }
                    else{
                        showNewMessage("Есть непрочитанные сообщения");        
                    }
                }
                else {
                    showNotification("<a href="+data.href+">Сообщение от <br>"+data.from+"</a>");                            
                }
                break;
            case "like":
                showNotification(data.message);
                break;
            case "friend":
                showNotification(data.message);
                break;
            case "post":
                showNotification(data.message);
                break;
            // Другие case
    
            default:
                break;
        }
    }
}

function showNotification(text) {
    var noty = document.getElementById("nodeInList");
    var list = document.getElementById("notificationList");

    var notification = document.createElement('div');
    notification.innerHTML = "<div class=\"notification\"><i class=\"delnotification fa fa-times\" aria-hidden=\'true\' title=\'Удалить\'></i><div class=\"notificationtext\">"+text+"</div>";

    insertAfter(notification, noty);

    setTimeout(function() {
        list.removeChild(notification);
    }, 3000);
}

function showNewMessage(text){
    var list = document.getElementById("notify-message-container");

    var notification = document.createElement('div');
    notification.innerHTML = "<div id=\"notify-message\"><i class=\"del-message-notification fa fa-times\" aria-hidden=\'true\' title=\'Удалить\'></i><div class=\"text-message-notification\">"+text+"</div>";
    
    var notified = document.getElementById("notify-message");
    if(!notified)
        list.appendChild(notification);
}

$(document).on('click','.delnotification', function() {
    var notification = $(this).parent().parent();        
    notification.remove();
});

$(document).on('click','.del-message-notification', function() {
    var notification = $(this).parent().parent();        
    notification.remove();
});

$(document).on('click','.text-message-notification', function() {
    var chatOutput = $("#chatOutput");
    chatOutput.stop().animate({
        scrollTop: chatOutput[0].scrollHeight
    }, 400);    
    $(this).parent().remove();
});
    
function insertAfter(elem, refElem) {
    return refElem.parentNode.insertBefore(elem, refElem.nextSibling);
  }

