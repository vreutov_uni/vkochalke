$(document).ready(function(){
    var isLoginForm = true;
    var isLoginFree = false;
    $("#loginForm").submit(function(e) {
        e.preventDefault();
        if (isLoginForm)
        {
            $.ajax({
                data: $("#loginForm").serialize() + '&action=login',
                method: $("#loginForm").attr('method'),
                url: $("#loginForm").attr('action'),
                beforeSend: function(){
                    $("#btLogin").text("Logging in...");
                },    
                success: function(data){
                    if (data == "OK")
                    {
                        $("#btLogin").text("Войти");
                        location.replace("/");
                    }
                    else
                    {
                        $("#pass").val('');
                        $("#result").slideDown();
                        $("#result").html(data);
                        $("#btLogin").text("Войти");
                    }
                },
                error: function(err){
                    $("#result").slideDown();
                    $("#result").html(err);
                } 
            })
        }
        else{
            $.ajax({
                data: $("#loginForm").serialize() + '&action=register',
                method: $("#loginForm").attr('method'),
                url: $("#loginForm").attr('action'),
                beforeSend: function(){
                    
                },    
                success: function(data){
                    if (data == "OK")
                    {
                        $("#result").text(data);
                        location.replace("/");
                    }
                },
                error: function(err){
                    $("#result").slideDown();
                    $("#result").html(err);
                } 
            })
        }
    });

    function check(){
        if(!isLoginForm){
            var password = $("#pass").val();
            var name = $("#name").val();
            var fam = $("#fam").val();
            if (isLoginFree && password.length > 3 && name.length > 1 && fam.length > 1){
                $("#btSub").removeAttr("disabled");
            }
            else{
                $("#btSub").attr("disabled", "disabled");
            }
        }
    }

    $("#login").on('input', function(){
        $(this).val(function(_, v){
            return v.replace(/\W+/g, '');
        });
        var login = $("#login").val();
        if(!isLoginForm){
            if (login.length > 3 && /^\w+$/.test(login))
            {
                $.ajax({
                    data: $("#login").serialize() + '&action=check_login',
                    method: "post",
                    url: "php/ajax.php",
                    success:function(data){
                        if (data == "OK"){
                            $("#result").text("Логин свободен");
                            isLoginFree = true;
                        }
                        else{
                            isLoginFree = false;
                            $("#result").text("Логин занят");
                        }
                        check();
                    },
                    error: function(err){
                        $("#result").slideDown();
                        $("#result").html(err);
                    } 
                }); 
            }
            else{
                $("#result").text("Введите логин/пароль больше 3х символов");                
                $("#btSub").prop("disabled", "true");
            }
        }
    });

    $("#pass").on('input', function(){
        $(this).val(function(_, v){
            return v.replace(/\s+/g, '');
        });
        check();
    });

    $("#fam").on('input', check);
    $("#name").on('input', check);

    $("#btFriends").click(function(){
        location.href = "friends.html";
    });

    $("#btProfile").click(function(){
        location.href = "index.html";
    });

    $("#exitbutton").click(function(){
        location.href = "login.html";
    });

    $("#a_create").click(function(){
        $(".reg").slideToggle("normal");
        $(".loginfo").fadeIn("normal");
        $(".reginfo").css("display","none");
        $("#btLogin").css("display","none");
        $("#btReg").fadeToggle("normal");
        $("#result").slideDown();
        $("#result").text("Введите логин/пароль больше 3х символов");
        $("#btSub").prop("disabled", "true");
        $("#login").val('');
        $("#pass").val('');
        isLoginForm = false;
    });
    $("#a_login").click(function(){
        $(".reg").slideToggle("normal");
        $(".loginfo").css("display","none");
        $(".reginfo").fadeIn("normal");
        $("#btLogin").fadeToggle("normal");
        $("#btReg").css("display","none");
        $("btReg").prop("disabled", "false");
        $("#result").slideUp();
        $("#login").val('');
        $("#pass").val('');
        $("#result").text("Неверный логин / пароль");
        $("#btSub").removeAttr("disabled");
        isLoginForm = true;
    });
});