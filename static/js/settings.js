
    var abouts = ['height', 'weight', 'benchPress', 'squat', 'deadlift', 'about'];
    var all_info = ['height', 'weight', 'benchPress', 'squat', 'deadlift', 'about', 'lastName', 'firstName', 'sex', 'birthday'];

    var isBlank = function(obj){
        return(!obj || $.trim(obj) === "");
    };
    
    $("#ava").click(function(){
        $(btLoad).trigger("click");
    });

    $("#loadText").click(function(){
        $(btLoad).trigger("click");
    });

    $("#btLoad").change(function(){
        var file = document.querySelector('input[type=file]').files[0];
        console.log(file);
        var formData = new FormData();
        formData.append('file', file);
        console.log("success");
        $.ajax({
            method: "post",
            type: "post",
            dataType: "text",
            cache: false,
            contentType: false,
            processData: false,
            url: "/php/upload.php",
            data: formData,
            beforeSend: console.log("sending..."), 
            success: function(data){
                alert(data);
                location.reload();
            },
            error: function(err){
                alert(err);
            }
        });
    });

    var begin_edit = function(edits, label) {
        edits.forEach(function(item) {
            var h = $(`[data-id^=${ item }]`);
            var text = h.text();
            h.replaceWith(`<input type='text' data-id='${ item }' value='${ text }'></input>`);
        });

        $ico = $("#" + label + "-edit");
        $ico.replaceWith("<i id='" + label + "-save' class='fa fa-floppy-o' aria-hidden='true'></i>");
    }

    var end_edit = function(edits, label) {
        var flag = false;
        var ajax_data = {};
        edits.forEach(function(item) {
            var about = $(`[data-id^=${ item }]`);
            if (item==="firstName" && isBlank(about.val()))
            {
                about.attr("placeholder","Имя не может быть пустым");
                flag = true;
            }
            if (item==="lastName" && isBlank(about.val()))
            {
                about.attr("placeholder","Фамилия не может быть пустой");
                flag = true;
            }
            if (item==="sex" && isBlank(about.val()))
            {
                about.attr("placeholder","Пол не может быть пустым");
                flag = true;
            }
            if (item==="birthday" && isBlank(about.val()))
            {
                about.attr("placeholder","Дата рождения не может быть пустой");
                flag = true;
            }

            ajax_data[item] = about.val();
            

        });
        
        if (flag) return;

        ajax_data['action'] = 'save_about';
        
        $.ajax({
            method: "post",
            url: "/php/ajax.php",
            data: ajax_data,
            beforeSend: function() {
                // $("#output").text("LOADING");
            },
            success: function() {
                $("#output").text("");

                edits.forEach(function(item) {
                    var about = $(`[data-id^=${ item }]`);
                    var text = about.val();
                    about.replaceWith(`<span data-id='${ item }'>${ text }</span>`)
                });

                $ico = $("#" + label + "-save");
                $ico.replaceWith("<i id='" + label + "-edit' class='fa fa-pencil' aria-hidden='true'></i>");
            },
            error: function(err){
                $("#output").text(err);
            }
        });
    }

    $(document).on('click', '#about-edit', function() {
        begin_edit(abouts, 'about');
    });

    $(document).on('click', '#about-save', function() {
        end_edit(abouts, 'about');        
    });

    $(document).on('click', '#all-edit', function() {
        begin_edit(all_info, 'all');
    });

    $(document).on('click', '#all-save', function() {
        end_edit(all_info, 'all');        
    });