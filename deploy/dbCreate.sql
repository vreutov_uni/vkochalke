-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.50 - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных vkochalke
DROP DATABASE IF EXISTS `vkochalke`;
CREATE DATABASE IF NOT EXISTS `vkochalke` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `vkochalke`;


-- Дамп структуры для таблица vkochalke.Achievements
DROP TABLE IF EXISTS `Achievements`;
CREATE TABLE IF NOT EXISTS `Achievements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Achievements: ~6 rows (приблизительно)
DELETE FROM `Achievements`;
/*!40000 ALTER TABLE `Achievements` DISABLE KEYS */;
INSERT INTO `Achievements` (`id`, `name`, `description`, `points`) VALUES
	(1, 'Мощная бицуха', 'Всю неделю качать бицуху', 3),
	(2, 'Орех', 'Всю неделю качать ягодицы', 3),
	(3, 'Красавчик', 'Выступить на соревнованиях по бодибилдингу', 1),
	(4, 'Тренер', 'Стать тренером', 1),
	(5, 'Бицуха-2017', 'Занять 1 место на соревнованиях по пауэрлифтингу', 1),
	(6, 'Мастер по железу', 'Качаться более 10 лет', 10);
/*!40000 ALTER TABLE `Achievements` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.CheckInGyms
DROP TABLE IF EXISTS `CheckInGyms`;
CREATE TABLE IF NOT EXISTS `CheckInGyms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKoch` int(11) NOT NULL,
  `idGym` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CheckInGyms_Gyms` (`idGym`),
  KEY `FK_CheckInGyms_Kochs` (`idKoch`),
  CONSTRAINT `FK_CheckInGyms_Gyms` FOREIGN KEY (`idGym`) REFERENCES `Gyms` (`id`),
  CONSTRAINT `FK_CheckInGyms_Kochs` FOREIGN KEY (`idKoch`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.CheckInGyms: ~9 rows (приблизительно)
DELETE FROM `CheckInGyms`;
/*!40000 ALTER TABLE `CheckInGyms` DISABLE KEYS */;
INSERT INTO `CheckInGyms` (`id`, `idKoch`, `idGym`, `date`) VALUES
	(1, 1, 1, '2017-10-10'),
	(2, 1, 4, '2013-09-05'),
	(3, 2, 4, '2015-10-12'),
	(4, 1, 3, '2017-04-08'),
	(5, 3, 1, '2017-07-17'),
	(6, 3, 2, '2017-08-20'),
	(7, 4, 4, '2007-03-28'),
	(8, 4, 3, '2014-05-30'),
	(9, 4, 1, '2016-11-10');
/*!40000 ALTER TABLE `CheckInGyms` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.Friends
DROP TABLE IF EXISTS `Friends`;
CREATE TABLE IF NOT EXISTS `Friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKoch1` int(11) NOT NULL,
  `idKoch2` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Friends_Kochs` (`idKoch1`),
  KEY `FK_Friends_Kochs1` (`idKoch2`),
  CONSTRAINT `FK_Friends_Kochs` FOREIGN KEY (`idKoch1`) REFERENCES `Kochs` (`id`),
  CONSTRAINT `FK_Friends_Kochs1` FOREIGN KEY (`idKoch2`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Friends: ~8 rows (приблизительно)
DELETE FROM `Friends`;
/*!40000 ALTER TABLE `Friends` DISABLE KEYS */;
INSERT INTO `Friends` (`id`, `idKoch1`, `idKoch2`) VALUES
	(1, 1, 2),
	(2, 1, 3),
	(3, 1, 4),
	(4, 2, 1),
	(5, 3, 1),
	(6, 4, 1),
	(7, 3, 4),
	(8, 4, 3);
/*!40000 ALTER TABLE `Friends` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.GetAchievements
DROP TABLE IF EXISTS `GetAchievements`;
CREATE TABLE IF NOT EXISTS `GetAchievements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKoch` int(11) NOT NULL,
  `idAchievement` int(11) NOT NULL,
  `progress` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_GetAchievements_Achievements` (`idAchievement`),
  KEY `FK_GetAchievements_Kochs` (`idKoch`),
  CONSTRAINT `FK_GetAchievements_Achievements` FOREIGN KEY (`idAchievement`) REFERENCES `Achievements` (`id`),
  CONSTRAINT `FK_GetAchievements_Kochs` FOREIGN KEY (`idKoch`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.GetAchievements: ~10 rows (приблизительно)
DELETE FROM `GetAchievements`;
/*!40000 ALTER TABLE `GetAchievements` DISABLE KEYS */;
INSERT INTO `GetAchievements` (`id`, `idKoch`, `idAchievement`, `progress`) VALUES
	(1, 1, 4, 1),
	(2, 1, 1, 1),
	(3, 1, 2, 1),
	(4, 1, 3, 1),
	(5, 1, 6, 4),
	(6, 2, 2, 1),
	(7, 3, 1, 1),
	(8, 4, 1, 1),
	(9, 4, 2, 1),
	(10, 4, 6, 10);
/*!40000 ALTER TABLE `GetAchievements` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.GymNews
DROP TABLE IF EXISTS `GymNews`;
CREATE TABLE IF NOT EXISTS `GymNews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idGym` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_GymNews_Gyms` (`idGym`),
  CONSTRAINT `FK_GymNews_Gyms` FOREIGN KEY (`idGym`) REFERENCES `Gyms` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.GymNews: ~2 rows (приблизительно)
DELETE FROM `GymNews`;
/*!40000 ALTER TABLE `GymNews` DISABLE KEYS */;
INSERT INTO `GymNews` (`id`, `idGym`, `data`, `text`) VALUES
	(1, 3, '2017-09-12 17:22:00', 'МЫ ЗАКРЫВАЕМСЯ! Филиал на б.Гагарина продолжает работу! Просим прощения за доставленные неудобства!'),
	(2, 2, '2017-09-20 14:31:00', '10 октября бесплатный вход для людей весом до 60 кг');
/*!40000 ALTER TABLE `GymNews` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.Gyms
DROP TABLE IF EXISTS `Gyms`;
CREATE TABLE IF NOT EXISTS `Gyms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `address` varchar(150) NOT NULL,
  `info` longtext NOT NULL,
  `ava` longtext,
  `isActive` tinyint(4) NOT NULL,
  `idOwner` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Gyms_Kochs` (`idOwner`),
  CONSTRAINT `FK_Gyms_Kochs` FOREIGN KEY (`idOwner`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Gyms: ~5 rows (приблизительно)
DELETE FROM `Gyms`;
/*!40000 ALTER TABLE `Gyms` DISABLE KEYS */;
INSERT INTO `Gyms` (`id`, `name`, `address`, `info`, `ava`, `isActive`, `idOwner`) VALUES
	(1, 'MegaGym', 'г.Пермь ул.Пермская д.62', 'Для тех, кто сделан из стали', 'gyms/gym2.jpg', 1, 1),
	(2, 'Мясодел', 'г.Пермь ул.Мясная д.1', 'Нарасти массу вместе с нами', 'gyms/gym4.jpg', 1, 1),
	(3, 'Alex Fitness', 'г.Пермь ул.Куйбышева д.66', 'Яркий фитнес!', 'gyms/gym1.jpg', 0, 1),
	(4, 'Alex Fitness', 'г.Пермь ул.бульвар Гагарина д.32', 'Яркий фитнес!', 'gyms/gym1.jpg', 1, 1),
	(5, 'У Михалыча', 'г.Пермь ул.Кузнецкая д.48', 'МЯСО МЯСО МЯСО', 'gyms/gym3.jpg', 1, 1);
/*!40000 ALTER TABLE `Gyms` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.Kochs
DROP TABLE IF EXISTS `Kochs`;
CREATE TABLE IF NOT EXISTS `Kochs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `sex` char(1) NOT NULL,
  `birthday` date NOT NULL,
  `ava` longtext,
  `about` longtext,
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `benchPress` double DEFAULT NULL,
  `deadlift` double DEFAULT NULL,
  `squat` double DEFAULT NULL,
  `isTrainer` tinyint(4) NOT NULL,
  `status` char(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Kochs: ~6 rows (приблизительно)
DELETE FROM `Kochs`;
/*!40000 ALTER TABLE `Kochs` DISABLE KEYS */;
INSERT INTO `Kochs` (`id`, `firstName`, `lastName`, `sex`, `birthday`, `ava`, `about`, `height`, `weight`, `benchPress`, `deadlift`, `squat`, `isTrainer`, `status`) VALUES
	(1, 'Коч', 'Кочков', 'м', '1983-10-10', 'users/ava3.jpg', 'варпол', 180, 180, 180, 180, 1515, 1, 'online'),
	(2, 'Жена', 'Коча', 'ж', '1994-02-13', 'users/ava2.jpg', 'Я жена коча', 160, 50, NULL, NULL, 100500, 0, 'ofline'),
	(3, 'Иван', 'Иванов', 'м', '2000-01-01', 'users/ava4.jpg', NULL, 180, 100, 150, 200, 180, 0, 'ofline'),
	(4, 'Виктор', 'Викторов', 'м', '1982-02-06', 'users/ava1.jpg', 'ВИТЯН', NULL, NULL, NULL, NULL, NULL, 0, 'ofline'),
	(6, '123456', '123456', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, ''),
	(9, 'Тест', 'Тестович', '', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '');
/*!40000 ALTER TABLE `Kochs` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.Likes
DROP TABLE IF EXISTS `Likes`;
CREATE TABLE IF NOT EXISTS `Likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPost` int(11) NOT NULL DEFAULT '0',
  `idKoch` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK__Posts` (`idPost`),
  KEY `FK__Kochs` (`idKoch`),
  CONSTRAINT `FK__Posts` FOREIGN KEY (`idPost`) REFERENCES `Posts` (`id`),
  CONSTRAINT `FK__Kochs` FOREIGN KEY (`idKoch`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Likes: ~1 rows (приблизительно)
DELETE FROM `Likes`;
/*!40000 ALTER TABLE `Likes` DISABLE KEYS */;
INSERT INTO `Likes` (`id`, `idPost`, `idKoch`) VALUES
	(1, 25, 2);
/*!40000 ALTER TABLE `Likes` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.Messages
DROP TABLE IF EXISTS `Messages`;
CREATE TABLE IF NOT EXISTS `Messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKochFrom` int(11) NOT NULL,
  `idKochTo` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `text` longtext NOT NULL,
  `isDeleted` tinyint(4) NOT NULL,
  `isReaded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_Messages_Kochs` (`idKochFrom`),
  KEY `FK_Messages_Kochs1` (`idKochTo`),
  CONSTRAINT `FK_Messages_Kochs` FOREIGN KEY (`idKochFrom`) REFERENCES `Kochs` (`id`),
  CONSTRAINT `FK_Messages_Kochs1` FOREIGN KEY (`idKochTo`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Messages: ~246 rows (приблизительно)
/*!40000 ALTER TABLE `Messages` DISABLE KEYS */;
INSERT INTO `Messages` (`id`, `idKochFrom`, `idKochTo`, `date`, `text`, `isDeleted`, `isReaded`) VALUES
	(1, 1, 2, '2017-10-10 11:33:00', 'Привет, белочка! Как дела?', 0, 1),
	(2, 2, 1, '2017-10-10 11:38:00', 'приветик, медвежонок:** у меня всё супер, а у тебя? пойдешь в зал сегодня?', 0, 1),
	(3, 1, 2, '2017-10-10 11:39:00', 'Угу. Сегодня будет куча народу, ты придешь?', 0, 1),
	(4, 1, 2, '2017-10-10 11:40:00', 'Обещаю сильно не приставать ;Р', 0, 1),
	(5, 3, 1, '2017-10-05 18:32:00', 'Коч, привет! Позвони, когда сможешь! Хочу обсудить с тобой спортпит', 0, 0),
	(6, 3, 4, '2017-10-05 19:10:00', 'я написал кочу, как вы сказали', 0, 0),
	(7, 1, 2, '2017-10-30 23:24:26', 'hahahah', 0, 1),
	(8, 1, 2, '2017-10-30 23:25:00', 'hahahah', 0, 1),
	(9, 1, 2, '2017-10-30 23:30:26', 'эй, ты', 0, 1),
	(10, 2, 1, '2017-10-30 23:30:34', 'э, чё надо', 0, 1),
	(11, 1, 2, '2017-10-30 23:31:37', 'ну ты и коч', 0, 1),
	(12, 2, 1, '2017-10-30 23:31:43', 'от коча слышу', 0, 1),
	(13, 1, 2, '2017-10-30 23:31:53', 'вот сейчас обидно было', 0, 1),
	(14, 2, 1, '2017-10-30 23:31:57', 'лол', 0, 1),
	(265, 2, 1, '2017-11-25 16:12:45', 'всё как я хотела', 0, 1),
	(266, 2, 1, '2017-11-25 17:08:53', 'привет', 0, 1),
	(267, 2, 1, '2017-11-25 17:15:42', 'супер', 0, 1),
	(268, 1, 2, '2017-11-25 17:41:40', 'класс', 0, 0);
/*!40000 ALTER TABLE `Messages` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.Posts
DROP TABLE IF EXISTS `Posts`;
CREATE TABLE IF NOT EXISTS `Posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKoch` int(11) NOT NULL,
  `idAutor` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `text` longtext NOT NULL,
  `isDeleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Posts_Kochs` (`idKoch`),
  KEY `FK_Posts_Kochs1` (`idAutor`),
  CONSTRAINT `FK_Posts_Kochs` FOREIGN KEY (`idKoch`) REFERENCES `Kochs` (`id`),
  CONSTRAINT `FK_Posts_Kochs1` FOREIGN KEY (`idAutor`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Posts: ~28 rows (приблизительно)
DELETE FROM `Posts`;
/*!40000 ALTER TABLE `Posts` DISABLE KEYS */;
INSERT INTO `Posts` (`id`, `idKoch`, `idAutor`, `date`, `text`, `isDeleted`) VALUES
	(1, 1, 1, '2017-09-12 12:40:00', 'Есть вопросы? Пиши в личку!', 0),
	(2, 3, 3, '2017-09-10 23:21:00', 'Хочу медаль!', 0),
	(3, 3, 3, '2017-09-10 18:02:00', 'Спорт это жизнь!', 0),
	(4, 1, 1, '2017-08-29 13:33:00', 'Хочешь подкачаться? Пиши!!!', 1),
	(5, 4, 4, '2017-10-10 03:28:00', 'Я МАШИНА', 0),
	(6, 1, 1, '2017-10-24 07:29:13', 'test message', 0),
	(7, 1, 1, '2017-10-24 08:01:45', 'ghjkghjk', 1),
	(11, 1, 1, '2017-10-24 08:07:53', 'test message', 1),
	(12, 1, 1, '2017-10-24 08:10:15', 'Я самая красивая!!!', 1),
	(13, 1, 1, '2017-11-07 09:07:09', 'asdfghjkl', 0),
	(14, 1, 1, '2017-11-07 09:07:54', '<script>alert(1);</script>', 1),
	(15, 1, 1, '2017-11-07 09:10:55', '<script>alert(2);</script>', 1),
	(16, 1, 1, '2017-11-07 09:15:06', 'asd', 0),
	(17, 1, 1, '2017-11-07 09:15:09', '&lt;script&gt;alert(1);&lt;/script&gt;', 0),
	(18, 1, 1, '2017-11-09 23:05:05', 'asd', 1),
	(19, 1, 1, '2017-11-13 21:23:01', '234325', 1),
	(20, 1, 1, '2017-11-13 22:09:02', '123', 1),
	(21, 1, 1, '2017-11-13 22:09:17', '123', 1),
	(22, 1, 1, '2017-11-13 22:10:10', '456', 1),
	(23, 1, 1, '2017-11-13 22:18:13', '789', 1),
	(24, 1, 1, '2017-11-13 22:18:22', '485', 1),
	(25, 1, 1, '2017-11-13 22:18:59', 'лолололололо новая запись', 0),
	(26, 3, 1, '2017-11-13 22:19:26', 'Привет, лошара', 1),
	(27, 3, 1, '2017-11-14 07:14:33', 'лох\n', 1),
	(28, 3, 1, '2017-11-14 07:18:19', 'asdf', 1),
	(29, 3, 1, '2017-11-14 07:20:09', 'asadads', 1),
	(30, 3, 1, '2017-11-14 07:22:16', 'gsdfgh', 1),
	(31, 2, 1, '2017-11-14 07:48:17', 'Лафффки', 0);
/*!40000 ALTER TABLE `Posts` ENABLE KEYS */;


-- Дамп структуры для таблица vkochalke.Users
DROP TABLE IF EXISTS `Users`;
CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idKoch` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(49) NOT NULL,
  `lastActive` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Users_Kochs` (`idKoch`),
  CONSTRAINT `FK_Users_Kochs` FOREIGN KEY (`idKoch`) REFERENCES `Kochs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы vkochalke.Users: ~6 rows (приблизительно)
DELETE FROM `Users`;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` (`id`, `idKoch`, `login`, `password`, `salt`, `lastActive`) VALUES
	(1, 1, 'koch', 'fc29e42f08ff360b30be4df42dcc7f73dd945d2a206273b37dfd7f6da89b4e7f', '7f1254a82f7345d965848420cca2c8af', '2017-11-21 10:47:27'),
	(2, 2, 'zaya94', '801a8ea8ebf50cba722d2956922eb93252f712c6aa65afabf5b1a7e471576b59', '1baa1802030a6bb2ccdc4cab3f61751b', '2017-11-21 09:14:24'),
	(3, 3, 'ivan', '0cec7bbc118b0591dc8a9ecd526afe9de6afc66ac2595ee506918df9cf68dd4c', 'ab8131dc4285b99c04d9872096931f73', '2017-11-21 08:51:13'),
	(4, 4, 'viktorrr', '7a72d595f42559148f2fc10f7ebddb14a1358c4543f37c9f37c7299595c9f411', '0115c3158b8d408f93e5557b6d5e527d', '2017-11-21 08:51:13'),
	(6, 6, '123456', '7ce3667a9ea06bdec339a040fd52b8a569d927d0d1656788af54714749b64fee', '833e1d39ad4b2d7123750d6050f2ef2e', '2017-11-21 08:51:13'),
	(7, 9, 'test', '15c697f7c66c542ef3b2431929304033d1352f19409c54e450355eafc5b1e73c', '801afabaedfd6ee1062d090cb2554878', '2017-11-21 08:51:13');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
